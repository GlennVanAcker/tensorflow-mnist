# for the sake of this tutorial, using compatibility with V1 of tensorflow
import tensorflow.compat.v1 as tf
import input_data
#disable v2 behaviour because it is not compatible with eager execution
tf.disable_v2_behavior()

#tensorboard logging
logPath = "../tb_logs/"

# add summaries for tensorboard visualization
def variable_summaries(var):
    with tf.name_scope("summaries"):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)

# helper function to pull in data from the Mnist site
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

#Using interactive session makes it the default session so we don't need to pass sess
sess = tf.InteractiveSession()

# data placeholder for 28 * 28 image data (28 * 28 = 784)
with tf.name_scope("Input"):
    dataPlaceholder = tf.placeholder(tf.float32, shape=[None, 784], name="images")

    # a 10 element vector, containing the predicted probability of each
    # digit class. ex [ 0.14, 0.8, 0...]
    predictedProbabilityVector = tf.placeholder(tf.float32, [None, 10], name="predicted_probability_vector")

# in the simple version, we did not have spacial information, everything was flattened into a single vector
# here we reshape the images as a 2d Vector (-1 is an flag saying make this a list 28 x 28)
with tf.name_scope("Reshaped_data"):
    image = tf.reshape(dataPlaceholder, [-1,28,28,1], name="image")
    tf.summary.image('input_image', image, 5)

# Relu activation function returns 0 if the value is less than 0, and the value if it is greater or equal to 0

def weight_variable(shape, name=None):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial, name=name)

def bias_variable(shape, name=None):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name=name)

def conv2d(img, weight, name=None):
    return tf.nn.conv2d(img, weight, strides=[1,1,1,1], padding="SAME", name=name)

def max_pool_2x2(img, name=None):
    return tf.nn.max_pool(img, ksize=[1,2,2,1],
                        strides=[1,2,2,1], padding="SAME", name=name)

#define layers
#1st convolution layer
# 32 features for each 5x5 patches of the image, 1 is the color depth of the image (black or white)
with tf.name_scope("ConvolutionLayer1"):
    with tf.name_scope('weights'):
        weight_conv1 = weight_variable([5,5,1,32], name="weight")
        variable_summaries(weight_conv1)
    with tf.name_scope('biases'):
        bias_conv1 = bias_variable([32], name="bias")
        variable_summaries(bias_conv1)
    #do convolution on images, add bias and push through relu activation
    convolution1_model = conv2d(image, weight_conv1) + bias_conv1
    tf.summary.histogram('convolution1_model', convolution1_model)
    h_conv1 = tf.nn.relu(convolution1_model, name="activation_function")
    tf.summary.histogram('convolution1_relu', h_conv1)
    # take results and run through pooling layer
    h_pool1 = max_pool_2x2(h_conv1, name="pool")

#2nd convolution layer
# process the 32 features from layer 1, return 64 features weights and biases
with tf.name_scope("ConvolutionLayer2"):
    with tf.name_scope('weights'):
        weight_conv2 = weight_variable([5,5,32,64], name="weight")        
        variable_summaries(weight_conv2)

    with tf.name_scope('biases'):
        bias_conv2 = bias_variable([64], name="bias")        
        variable_summaries(bias_conv2)
    #do convolution from 1st layer pool, add bias and push through relu activation
    convolution2_model = conv2d(h_pool1, weight_conv2) + bias_conv2
    tf.summary.histogram('convolution2_model', convolution2_model)
    h_conv2 = tf.nn.relu(convolution2_model, name="activation_function")
    tf.summary.histogram('convolution2_relu', h_conv2)
    # take results and run through pooling layer
    h_pool2 = max_pool_2x2(h_conv2, name="pool")

#make a fully connection layer
# 7 by 7 image, 64 features, 1024 neurons
with tf.name_scope("fullyConnectedLayer"):
    weights_fullyConnected = weight_variable([7 * 7 * 64,  1024], name="weight")
    bias_fullyConnected = bias_variable([1024], name="bias")

    # connect output of pooling layer 2 as input to fully connected layer
    # flatten in 1 long layer:
    h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
    h_fullyConnected = tf.nn.relu(tf.matmul(h_pool2_flat, weights_fullyConnected) + bias_fullyConnected, name="activation_function")

# dropout some neurons to reduce overfitting (i.e. add random confusion to provided images to strengthen the prediction algorithm)
keep_probability = tf.placeholder(tf.float32, name="keep_probabilty")
h_fullyConnected_drop = tf.nn.dropout(h_fullyConnected, keep_probability)

#readout layer
with tf.name_scope("ReadoutLayer"):
    weights_fullyConnected2 = weight_variable([1024, 10], name="weight")
    bias_fullyConnected2 = bias_variable([10], name="bias")

# define model
convolution_model = tf.matmul(h_fullyConnected_drop, weights_fullyConnected2) + bias_fullyConnected2

# measure loss and minimize loss
with tf.name_scope("Cross_Entropy"):
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=predictedProbabilityVector, logits=convolution_model))

# loss optimization
with tf.name_scope("Loss"):
    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

with tf.name_scope("Accuracy"):
    # correct prediction
    correct_prediction = tf.equal(tf.argmax(convolution_model, 1), tf.argmax(predictedProbabilityVector, 1))
    # accuracy measurement
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

tf.summary.scalar("cross_entropy_scl", cross_entropy)
tf.summary.scalar("training_accuracy", accuracy)

# tensorboard merge summaries
summarize_all = tf.summary.merge_all()

# Initialize all of the variables
sess.run(tf.global_variables_initializer())

# tensorboard write the default graph
tbWriter = tf.summary.FileWriter(logPath, sess.graph)

# train model
import time
num_steps = 3000
display_every = 100

# start the timer
start_time = time.time()
end_time = time.time()

for i in range(num_steps):
    batch = mnist.train.next_batch(50)
    _, summary = sess.run([train_step, summarize_all], feed_dict={dataPlaceholder: batch[0], predictedProbabilityVector: batch[1], keep_probability: 0.5})

    # periodic status display
    if i%display_every == 0:
        train_accuracy = accuracy.eval(feed_dict={
            dataPlaceholder: batch[0], predictedProbabilityVector: batch[1], keep_probability: 1.0})
        end_time = time.time()
        print("step {0}, elapsed time {1:.2f} seconds, training accuracy {2:.3f}%".format(i, end_time-start_time, train_accuracy *100))
        #write summary to log
        tbWriter.add_summary(summary, i)

end_time = time.time()
print("Total time for training for {0} batches : {1:.2f} seconds%".format(i+1, end_time-start_time))

print("Test accuracy {0:.2f}%".format(accuracy.eval(feed_dict={
    dataPlaceholder: mnist.test.images, predictedProbabilityVector: mnist.test.labels, keep_probability: 1.0}) * 100))

sess.close()