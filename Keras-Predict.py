import tensorflow as tf

from keras.models import load_model
from keras.preprocessing.image import load_img, img_to_array
import os
import numpy as np

# load model
model_path = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "model" + os.path.sep + "keras_model"
model = load_model(model_path)

# load input
image = load_img('Images/img_39.jpg', color_mode="grayscale")
image = image - np.min(image)
image = image/np.max(image)
imageAsArray = img_to_array(image, data_format=None, dtype=None)


#lets print out the image values (just for fun)
print("loaded:")
for i in range(len(imageAsArray)):
    for j in range(len(imageAsArray[i])):
        if(imageAsArray[i][j] < .8):
            print(" ", end=" ")
        else:
            print("X", end=" ")
    print('\n')

#make prediction
reshapedImage = imageAsArray.reshape(1,28,28,1)
prediction = model.predict(reshapedImage)
print(prediction)
print("predicted number: " + str(np.argmax(prediction[0])))
