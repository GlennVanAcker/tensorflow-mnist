# for the sake of this tutorial, using compatibility with V1 of tensorflow
import tensorflow as tf
import input_data
import os
import pathlib

# helper function to pull in data from the Mnist site
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

# tflearn libraries
import tflearn
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.normalization import local_response_normalization
from tflearn.layers.estimator import regression


# image shape constants
image_rows = 28
image_cols = 28

# read and reshape the data
train_images = mnist.train.images.reshape(mnist.train.images.shape[0], image_rows, image_cols, 1)
test_images = mnist.test.images.reshape(mnist.test.images.shape[0], image_rows, image_cols, 1)

num_classes = 10
keep_probability = 0.5

# define input shape coming into the NN
input = input_data(shape=[None, 28,28,1], name="input")

# create first 2d convolution layer
network = conv_2d(input, nb_filter=32, filter_size=3, activation="relu", regularizer="L2")

#take results and run through max_pool
network = max_pool_2d(network, 2)

#create second layer
network = conv_2d(network, nb_filter=64, filter_size=3, activation="relu", regularizer="L2")
network = max_pool_2d(network, 2)

#fully connected layer
network = fully_connected(network, 128, activation="tanh")

#dropout layer
network = dropout(network, keep_probability)

# readout layer
network = fully_connected(network, 10, activation="softmax")

# set loss and measurement, optimizer
network = regression(network, optimizer="adam", learning_rate=0.01, loss="categorical_crossentropy", name="target")

#Training
num_epoch = 2
model = tflearn.DNN(network, tensorboard_verbose=0)
model.fit({"input": train_images}, {"target": mnist.train.labels}, n_epoch=num_epoch,
            validation_set=({"input": test_images}, {"target": mnist.test.labels}),
            show_metric=True, run_id="TFLearn_DeepMNIST")

model_path = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "model" + os.path.sep + "tflearn_model"
print("saving model to: " + model_path)
model.save(model_path)
