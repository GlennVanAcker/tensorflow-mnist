import tensorflow as tf
import input_data
import pathlib

from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras import backend as K

import os

#disable v2 behaviour because it is not compatible with eager execution
tf.compat.v1.disable_v2_behavior()

# helper function to pull in data from the Mnist site
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

#Using interactive session makes it the default session so we don't need to pass sess
sess = tf.compat.v1.InteractiveSession()

image_rows = 28
image_cols = 28

#reshape data with mnist libraries to 28 * 28 * 1
train_images = mnist.train.images.reshape(mnist.train.images.shape[0], image_rows, image_cols, 1)
test_images = mnist.test.images.reshape(mnist.test.images.shape[0], image_rows, image_cols, 1)

#layer values for keras
num_filters = 32 # convolution filters
max_pool_size = (2,2) # shape of maxPool

conv_kernel_size = (3,3)
image_shape = (28,28,1)
num_classes = 10
drop_probability = .5   # dropout to reduce overfitting

#### create neural net structure using keras ####

# define model
model = Sequential()

# define layers

# layer 1: 
# border_mode and input_shape only on first layer
# border_mode = value restricts convolution to only where the input and the filter fully overlap
model.add(Convolution2D(num_filters, conv_kernel_size[0], conv_kernel_size[1], padding='valid', input_shape=image_shape))
#input_shape: only for the first layer, in the other layers, keras figures this out by looking at the output of the previous layer

# push through relu activation function
model.add(Activation("relu"))

# take results and run through max_pool
model.add(MaxPooling2D(pool_size=max_pool_size))

# second convolution layer
model.add(Convolution2D(num_filters, conv_kernel_size[0], conv_kernel_size[1]))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=max_pool_size, padding="same"))

# fully connected layer
model.add(Flatten()) # convert tensor from 4D to 2D representation
model.add(Dense(128)) # connect neurons of previous layers to all 128 layers in the fully connected layer
model.add(Activation("relu"))

# Add dropout layer to reduce overfitting (i.e. dropout some neurons)
model.add(Dropout(drop_probability))

#readout layer
model.add(Dense(num_classes))
model.add(Activation("softmax"))

#compile layer. set loss and measurement, optimizer and metric used to evaluate loss
model.compile(loss="categorical_crossentropy",
            optimizer="adam",
            metrics=["accuracy"])

# train model

batch_size= 128
num_epoch = 5

model.fit(train_images, mnist.train.labels, batch_size=batch_size, nb_epoch=num_epoch,
            verbose=1, validation_data=(test_images, mnist.test.labels))

model_path = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "model" + os.path.sep + "keras_model"
print("saving model to: " + model_path)
model.save(model_path, True, True)

# if you want to save to json 
# model.to_json()