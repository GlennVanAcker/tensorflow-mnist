# for the sake of this tutorial, using compatibility with V1 of tensorflow
import tensorflow.compat.v1 as tf
import input_data
#disable v2 behaviour because it is not compatible with eager execution
tf.disable_v2_behavior()

# helper function to pull in data from the Mnist site
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

# data placeholder for 28 * 28 image data (28 * 28 = 784)
dataPlaceholder = tf.placeholder(tf.float32, shape=[None, 784])

# a 10 element vector, containing the predicted probability of each
# digit class. ex [ 0.14, 0.8, 0...]
predictedProbabilityVector = tf.placeholder(tf.float32, [None, 10])

# define weights and biases
weights = tf.Variable(tf.zeros([784, 10]))
biases = tf.Variable(tf.zeros([10]))

### define prediction ###

#define the model (softmax is an exponential activation function), matrix multiplication of data and weights + biases through activation function
model = tf.nn.softmax(tf.matmul(dataPlaceholder, weights) + biases)

# loss measurement (compare prediction with actual number in the data) = cross entropy
cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=predictedProbabilityVector, logits=model))

# each training step in gradient decent we want to minimize cross entropy
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

### start training ###
# init variables
init = tf.global_variables_initializer()

#create an interactive session that can span multiple code blocks
#session needs to be explicitely closed
sess = tf.Session()

# perform the initialization
sess.run(init)

# train a 1000 times
for i in range(1000):
    image, digitClass = mnist.train.next_batch(100) # get 100 random data points
    sess.run(train_step, feed_dict={dataPlaceholder: image, predictedProbabilityVector: digitClass}) # do optimization with this data

# evaluate how well the model did. do this by comparing the digit with the highest probability in
# actual (model) and predicted (predictedProbabilityVector)
correct_prediction = tf.equal(tf.argmax(model, 1), tf.argmax(predictedProbabilityVector, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

#display the results:
test_accuracy = sess.run(accuracy, feed_dict={dataPlaceholder: mnist.test.images, predictedProbabilityVector: mnist.test.labels})
print("Test accuracy: {0}%".format(test_accuracy * 100.0))

sess.close()

